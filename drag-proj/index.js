(function() {
    'use strict';
    
    let boxes = document.querySelectorAll('.box');
    let input = document.querySelectorAll('input');
    var oldBody = document.getElementsByTagName('body');
    var isDown = false;
    var offset = [0,0];
    var div = {};
    var mousePosition;

    document.addEventListener('click', createDiv);

    function createDiv(event) {

        let divType = event.target.innerText;
        console.log(input);

        if(divType === 'BOX' && event.target.nodeName === 'BUTTON') {
            createNewBox();
        }else if (divType === 'THREAD' && event.target.nodeName === 'BUTTON'){
            createNewThread();
        }else if (divType === 'ARROW' && event.target.nodeName === 'BUTTON'){
            createNewArrow();
        } else {
        }
    }


    function createNewBox() {
        var newBox = document.createElement('div');
        let colors = getRandomColors();

        newBox.className = 'box flex movable';

        oldBody[0].appendChild(newBox);
        //style
        newBox.innerText = input[0].value;
        newBox.style.backgroundColor = colors.back;    
        newBox.style.color = colors.front;
    }

    function createNewThread() {
        var newThread = document.createElement('div');
        
        newThread.className = 'thread movable';
        oldBody[0].appendChild(newThread);

        newThread.innerText = input[0].value;
    }

    function createNewArrow() {
        var newArrow = document.createElement('div');
        var newArrowText = document.createElement('div');

        newArrow.className = 'arrow-down movable';
        newArrowText.className = 'arrow-text';
        newArrow.appendChild(newArrowText);
        oldBody[0].appendChild(newArrow);
        newArrowText.innerText = input[0].value;

    }




    //create new objects














    // dragging code

    document.addEventListener('mousedown', click, true);
    document.addEventListener('mouseup', handleMouseUp, true);
    document.addEventListener('mousemove', mousePosition, true);
    
    // target object
    function click(event) {
        if (event.target.classList.contains('movable')) {
           div = event.target;
           isDown = true;
           div.style.boxShadow = '0px 2px 6px 0px black';           
        offset = [
            div.offsetLeft - event.clientX,
            div.offsetTop - event.clientY
        ];
        }
    }

    function handleMouseUp(event) {
        isDown = false;
        div.style.boxShadow = '';
        div.style.cursor = 'pointer';
    }

    function mousePosition(event) {
        event.preventDefault();
        if (isDown) {
            mousePosition = {
                x : event.clientX,
                y : event.clientY
            };
            div.style.left = (mousePosition.x + offset[0]) + 'px';
            div.style.top  = (mousePosition.y + offset[1]) + 'px';
            div.style.cursor = 'move';
        }
    };
    //end of dragging code

    function placeBoxes() {
        
        for(let i = 0; i < boxes.length; i++) {
            let box = boxes[i];
            box.style.position = 'absolute';
            box.style.top = 66 + i * 66 + 'px';
            box.style.width = '1px';
            box.style.height = '50px';
            box.style.margin = '1px';
            box.style.textAlign = 'center';
        }
    }
    placeBoxes();

    /**
     * Generate an object containing a random color and sutible text color to use on it.
     * More on this can be found here: https://www.w3.org/TR/AERT#color-contrast
     * 
     * @return {Object} - {back, front} background color and forground color.
     */
    function getRandomColors() {
        let r = Math.floor(Math.random()*255);
        let g = Math.floor(Math.random()*255);
        let b = Math.floor(Math.random()*255);

        let brightness = ((r*299) + (g*587) + (b*114)) / 1000;

        return {
            back: 'rgb('+r+','+g+','+b+')',
            front: (brightness>127) ? '#000' : '#fff'
        };
    }

    function colorBoxes() {

        
        for(let i = 0; i < boxes.length; i++){
            let color = getRandomColors();
            boxes[i].style.backgroundColor = '' + color.back;
            boxes[i].style.color = '' + color.front;
        }
    }

    colorBoxes();
}());